from bonus_system import calculateBonuses
import pytest

@pytest.mark.parametrize(
    "program,number,result",
    [
        ("Standard",   9999,       0.5),
        ("Standard",  10000, 1.5 * 0.5),
        ("Standard",  49999, 1.5 * 0.5),
        ("Standard",  50000,   2 * 0.5),
        ("Standard",  99999,   2 * 0.5),
        ("Standard", 100000, 2.5 * 0.5),
        ("Standard", 100001, 2.5 * 0.5),

        ("Premium",    9999,       0.1),
        ("Premium",   10000, 1.5 * 0.1),
        ("Premium",   49999, 1.5 * 0.1),
        ("Premium",   50000,   2 * 0.1),
        ("Premium",   99999,   2 * 0.1),
        ("Premium",  100000, 2.5 * 0.1),
        ("Premium",  100001, 2.5 * 0.1),

        ("Diamond",    9999,       0.2),
        ("Diamond",   10000, 1.5 * 0.2),
        ("Diamond",   49999, 1.5 * 0.2),
        ("Diamond",   50000,   2 * 0.2),
        ("Diamond",   99999,   2 * 0.2),
        ("Diamond",  100000, 2.5 * 0.2),
        ("Diamond",  100001, 2.5 * 0.2),

        ("",          10000,         0),
        ("ababba",    10000,         0),
        ("123",       10000,         0),
        ("&|^~",      10000,         0),
    ],
)

def test_calculateBonuses(program, number, result):
    if result == "Error":
        with pytest.raises(TypeError):
            calculateBonuses(program, number)
    else:
        precision = 9
        assert round(calculateBonuses(program, number), precision) == round(
            result, precision
        )
