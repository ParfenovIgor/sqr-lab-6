# Lab6 -- Mutation testing 

[![pipeline status](https://gitlab.com/ParfenovIgor/sqr-lab-4/badges/master/pipeline.svg)](https://gitlab.com/ParfenovIgor/sqr-lab-6/-/commits/main)

## Author

Igor Parfenov

Contact: [@Igor_Parfenov](https://t.me/Igor_Parfenov)

## Mutation testing

There are two files with tests:

* `test_calculator.py`
* `test_bonus_system.py`

How to setup:

* Create virtual environment: `python3 -m venv env`, `source env/bin/activate`
* Install dependencies: `pip3 install -r requirements.txt`
* Run *Mutatest*: `mutatest --src ./src`
